import '../styles/index.scss';
import * as moment from 'moment';

console.log('webpack starterkit');
console.log(moment().format('MMMM Do YYYY, h:mm:ss a'));

//Function for display
function Display(input)
{
    document.getElementById("result").value+=input;
}
global.Display=Display;

//Function that calculate the value rounded to 2 decimals
function Calculate()
{
    //Giving the user an error if he enter an invalid input
    try
    {
        let userinput = document.getElementById("result").value;
        let calculated = Math.round(eval(userinput)*100)/100;
        document.getElementById("result").value = calculated;
    }
    catch(err)
    {
        document.getElementById("result").value = "invalid input";
    }
}
global.Calculate=Calculate;

//Function that clear the display
function Clear()
{
    document.getElementById("result").value = "";
}
global.Clear=Clear;

//Task 5
function ShowResult(input)
{
    let rng = Math.floor(Math.random() * 3);
    if (rng == 1)
    {
        if(input === 1)
        {
            document.getElementById("You").value = "You chose rock";
            document.getElementById("Him").value = "The computer chose rock";
            document.getElementById("RPSResult").value = "It is a draw";
        }
        else if(input === 2)
        {
            document.getElementById("You").value = "You chose paper";
            document.getElementById("Him").value = "The computer chose rock";
            document.getElementById("RPSResult").value = "You win";
        }
        else
        {
            document.getElementById("You").value = "You chose scissor";
            document.getElementById("Him").value = "The computer chose rock";
            document.getElementById("RPSResult").value = "You lose";
        }
    }
    else if(rng === 2)
    {
        if(input === 1)
        {
            document.getElementById("You").value = "You chose rock";
            document.getElementById("Him").value = "The computer chose paper";
            document.getElementById("RPSResult").value = "You lose";
        }
        else if(input === 2)
        {
            document.getElementById("You").value = "You chose paper";
            document.getElementById("Him").value = "The computer chose paper";
            document.getElementById("RPSResult").value = "It is a draw";
        }
        else
        {
            document.getElementById("You").value = "You chose scissor";
            document.getElementById("Him").value = "The computer chose paper";
            document.getElementById("RPSResult").value = "You win";
        }
    }
    else
    {
        if(input === 1)
        {
            document.getElementById("You").value = "You chose rock";
            document.getElementById("Him").value = "The computer chose scissor";
            document.getElementById("RPSResult").value = "You win";
        }
        else if(input === 2)
        {
            document.getElementById("You").value = "You chose paper";
            document.getElementById("Him").value = "The computer chose scissor";
            document.getElementById("RPSResult").value = "You lose";
        }
        else
        {
            document.getElementById("You").value = "You chose scissor";
            document.getElementById("Him").value = "The computer chose scissor";
            document.getElementById("RPSResult").value = "It is a draw";
        }
    }
}
global.ShowResult=ShowResult;

//Front end task 1

let number1 = 0;
let number2 = 1;
let number3 = number1 + number2;
let counter = 0;
while (counter<50)
{
    console.log(number1);
    number1 = number2;
    number2 = number3;
    number3 = number1 + number2;
    counter++;
}


//FrontTask2


let count = 1;
while (count<51)
{
    if(count % 15 == 0)
    {
        console.log("FizzBuzz");
    }
    else if(count % 3 == 0)
    {
        console.log("Fizz");
    }
    else if(count % 5 == 0)
    {
        console.log("Buzz");
    }
    count++;
}

//FrontTask3

moment().format('MMMM Do YYYY, h:mm:ss a');